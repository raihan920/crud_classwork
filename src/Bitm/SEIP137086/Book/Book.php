<?php
namespace App\Bitm\SEIP137086\Book;
use App\Bitm\SEIP137086\Book\Utility;
use App\Bitm\SEIP137086\Book\Message;

class Book{
    public $id="";
    public $title="";
    public $conn;


    public function prepare($data=""){
        if(array_key_exists("title",$data)){
            $this->title=$data['title'];
        }
        if(array_key_exists("id",$data)){
            $this->id=$data['id'];
        }

        //echo  $this;

    }

    public function __construct()
    {
        $this->conn=mysqli_connect("localhost","root","","atomicproject137086") or die("Database connection failed");
    }

    public function store(){
        $query="INSERT INTO `atomicproject137086`.`book` (`title`) VALUES ('".$this->title."')";
        //echo $query;

        $result=mysqli_query($this->conn,$query);
        if($result){
            Message::message("
<div class=\"alert alert-success\">
  <strong>Success!</strong> Data has been stored successfully.
</div>");
            Utility::redirect("index.php");
        }
        else {
            echo "Error";
        }
    }

 public function index(){
     $_allBook=array();
     $query="SELECT * FROM `book`";
     $result= mysqli_query($this->conn,$query);
     while($row=mysqli_fetch_object($result)){
         $_allBook[]=$row;
     }

     return $_allBook;


 }
    public function view(){
        $query="SELECT * FROM `book` WHERE `id`=".$this->id;
        $result= mysqli_query($this->conn,$query);
        $row= mysqli_fetch_object($result);
        return $row;
    }
    public function update(){
        $query="UPDATE `atomicproject137086`.`book` SET `title` = '".$this->title."' WHERE `book`.`id` = ".$this->id;
        //echo $query;

        $result=mysqli_query($this->conn,$query);
        if($result){
            Message::message("
                    <div class=\"alert alert-info\">
                      <strong>Success!</strong> Data has been updated  successfully.
                    </div>");
            Utility::redirect("index.php");
        }
        else {
            echo "Error";
        }
    }




}