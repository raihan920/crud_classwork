<?php
namespace App\Bitm\SEIP137086\Email;

use App\Bitm\SEIP137086\Email\Utility;
use App\Bitm\SEIP137086\Email\Message;

class Email
{
    public $id = "";
    public $email = "";
    public $conn;


    public function prepare($data = "")
    {
        if (array_key_exists("email", $data)) {
            $this->email = $data['email'];
        }
        if (array_key_exists("id", $data)) {
            $this->id = $data['id'];
        }

        //echo  $this;

    }

    public function __construct()
    {
        $this->conn = mysqli_connect("localhost", "root", "", "atomicproject137086") or die("Database connection failed");
    }

    public function store()
    {
        $query = "INSERT INTO `atomicproject137086`.`email` (`email`) VALUES ('" . $this->email . "')";
        //echo $query;

        $result = mysqli_query($this->conn, $query);
        if ($result) {
            Message::message("
                <div class=\"alert alert-success\">
                  <strong>Success!</strong> Email has been stored successfully.
                </div>");
            Utility::redirect("index.php");
        } else {
            echo "Error";
        }
    }

    public function index()
    {
        $_allEmail = array();
        $query = "SELECT * FROM `Email`";
        $result = mysqli_query($this->conn, $query);
        while ($row = mysqli_fetch_object($result)) {
            $_allEmail[] = $row;
        }

        return $_allEmail;


    }

    public function view()
    {
        $query = "SELECT * FROM `email` WHERE `id`=" . $this->id;
        $result = mysqli_query($this->conn, $query);
        $row = mysqli_fetch_object($result);
        return $row;
    }

    public function update()
    {
        $query = "UPDATE `atomicproject137086`.`email` SET `email` = '" . $this->email . "' WHERE `email`.`id` = " . $this->id;
        //echo $query;

        $result = mysqli_query($this->conn, $query);
        if ($result) {
            Message::message("
                    <div class=\"alert alert-info\">
                      <strong>Success!</strong> Email has been updated  successfully.
                    </div>");
            Utility::redirect("index.php");
        } else {
            echo "Error";
        }
    }
}