<?php
    session_start();
    include_once('../../../vendor/autoload.php');
    use App\Bitm\SEIP137086\Email\Email;
    use App\Bitm\SEIP137086\Email\Utility;
    use App\Bitm\SEIP137086\Email\Message;

      $email= new Email();
      $allEmail=$email->index();
        //Utility::d($allEmail);
?>

    <!DOCTYPE html>
    <html>
    <head>
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <link rel="stylesheet" href="http://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/css/bootstrap.min.css">
        <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.12.2/jquery.min.js"></script>
        <script src="http://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/js/bootstrap.min.js"></script>

    </head>
    <body>

    <div class="container">
        <h2>All Email List</h2>
        <a href="create.php" class="btn btn-primary" role="button">Create again</a>
        <div id="message">
        <?php if((array_key_exists('message',$_SESSION)&& (!empty($_SESSION['message'])))) {
            echo Message::message();
        }
        ?>
        </div>
        <div class="table-responsive">
            <table class="table">
                <thead>
                <tr>
                    <th>#</th>
                    <th>ID</th>
                    <th>Email Address</th>
                    <th>Action</th>

                </tr>
                </thead>
                <tbody>
                <tr>
                    <?php
                    $sl=0;
                    foreach($allEmail as $email){
                        $sl++; ?>
                    <td><?php echo $sl?></td>
                    <td><?php echo $email-> id?></td>
                    <td><?php echo $email->email?></td>
                    <td><a href="view.php?id=<?php echo $email-> id ?>" class="btn btn-primary" role="button">View</a>
                        <a href="edit.php?id=<?php echo $email-> id ?>"  class="btn btn-info" role="button">Edit</a>
                        <a href="delete.php" class="btn btn-danger" role="button">Delete</a>
                    </td>

                </tr>
                <?php }?>


                </tbody>
            </table>
        </div>
    </div>
    <script>
        $('#message').show().delay(2000).fadeOut();
    </script>

    </body>
    </html>
