<?php
//var_dump($_GET);
include_once ('../../../vendor/autoload.php');
use App\Bitm\SEIP137086\Email\Email;
use App\Bitm\SEIP137086\Email\Utility;

$email= new Email();
$email->prepare($_GET);
$singleItem=$email->view();
//Utility::d($singleItem);
?>

<!DOCTYPE html>
<html lang="en">
<head>
    <title>Edit Subscriber</title>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="stylesheet" href="http://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/css/bootstrap.min.css">
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.12.2/jquery.min.js"></script>
    <script src="http://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/js/bootstrap.min.js"></script>
</head>
<body>

<div class="container">
    <h2>Edit Subscriber</h2>
    <form role="form" method="post" action="update.php">
        <div class="form-group">
            <label>Edit Subscriber Email:</label>
            <input type="hidden" name="id" id="id" value="<?php echo $singleItem->id?>">
            <input type="email" name="email" class="form-control" id="email" placeholder="Enter Email Address" value="<?php echo $singleItem->email?>">
        </div>
        <button type="submit" class="btn btn-default">Update</button>
    </form>
</div>

</body>
</html>
